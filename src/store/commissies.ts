import axios from 'axios';
import { getAuthHeader } from './googleauth';
import { Module } from 'vuex';
import { IRootStore } from './index';

export interface ICommissiesStore {
  commissies: Commissie[];
}

type Commissie = any;

export default <Module<ICommissiesStore, IRootStore>>{
  namespaced: true,
  state: {
    commissies: [],
  },
  mutations: {
    setcommissies (state, commissies) {
      state.commissies.splice(0, state.commissies.length);
      state.commissies.push.apply(commissies);
    },
    setcommissie (state, commissie) {
      const index = state.commissies.indexOf(state.commissies.find(l => l.Naam === commissie.Naam));
      if (index === -1) {
        state.commissies.push(commissie);
      } else {
        state.commissies.splice(index, 1, commissie);
      }
    },
    deletecommissie (state, naam) {
      const index = state.commissies.indexOf(state.commissies.find(l => l.Naam === naam));
      state.commissies.splice(index, 1);
    },
  },
  actions: {
    async updatecommissie ({ commit, state }, commissie) {
      commit('startloading', { reason: 'updatecommissie' }, { root: true });
      try {
        await axios.post(`/api/commissies/${commissie.Naam}`, commissie, { headers: { Authorization: getAuthHeader() } });
        commit('setcommissie', commissie);
      } catch (e) {
        console.error(e);
      } finally {
        commit('stoploading', null, { root: true });
      }
    },
    async deletecommissie ({ commit, state }, naam) {
      commit('startloading', { reason: 'deletecommissie' }, { root: true });
      try {
        await axios.delete(`/api/commissies/${naam}`, { headers: { Authorization: getAuthHeader() } });
        commit('deletecommissie', naam);
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploading', null, { root: true });
      }
    },
    async loadcommissies ({ dispatch, commit, state }) {
      commit('startloading', { reason: 'loadcommissie' }, { root: true });
      try {
        const { data } = await axios.get('/api/commissies', { headers: { Authorization: getAuthHeader() } });
        commit('setcommissies', data);
      } finally {
        commit('stoploading', { reason: 'loadcommissie' }, { root: true });
      }
    },
  },
};
