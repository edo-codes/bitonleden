import Vue from 'vue';
import Vuex from 'vuex';

import leden, { ILedenState } from './leden';
import commissies, { ICommissiesStore } from './commissies';
import mailinglijsten, { IMailinglijstenState } from './mailinglijsten';
import googleauth, { IGoogleAuthState } from './googleauth';

Vue.use(Vuex);

export interface IRootStore {
  loading: string[]
}

export default new Vuex.Store<IRootStore>({
  modules: {
    leden,
    commissies,
    mailinglijsten,
    googleauth,
  },
  state: {
    loading: [],
  },
  mutations: {
    startloading (state, { reason }) {
      state.loading.push(reason);
    },
    stoploading (state, { reason }) {
      state.loading.splice(state.loading.indexOf(reason));
    },
  },
  getters: {
    isLoading (state) {
      return state.loading.length > 0;
    },
  },
});
