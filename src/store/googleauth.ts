import * as config from '../../config.js';
import { Module } from 'vuex';
import { IRootStore } from './index';

export interface IGoogleAuthState {
  inited: boolean;
  signedin: boolean;
  user?: string;
}

export default <Module<IGoogleAuthState, IRootStore>>{
  namespaced: true,
  state: {
    inited: false,
    signedin: false,
    user: undefined,
  },
  mutations: {
    setinited (state, inited) {
      state.inited = inited;
    },
    setsignedin (state, signedin) {
      state.signedin = signedin;
    },
    setuser (state, user) {
      state.user = user;
    },
  },
  actions: {
    async init ({ commit, state, dispatch }) {
      commit('startloading', { reason: 'init' }, { root: true });
      gapi.load('client:auth2', {
        callback: async () => {
          gapi.client.init({
            'apiKey': config.gapi_api_key,
            'discoveryDocs': [
              'https://www.googleapis.com/discovery/v1/apis/admin/directory_v1/rest',
              'https://www.googleapis.com/discovery/v1/apis/admin/reports_v1/rest',
            ],
            'clientId': config.gapi_client_id,
            'scope': [
              'https://www.googleapis.com/auth/admin.directory.group',
              'https://www.googleapis.com/auth/admin.directory.group.member',
              'https://www.googleapis.com/auth/admin.reports.audit.readonly',
            ].join(' '),
            'hosted_domain': 'biton.nl'
          }).then(() => {
            const auth = gapi.auth2.getAuthInstance();
            commit('setsignedin', auth.isSignedIn.get());
            if (state.signedin) commit('setuser', auth.currentUser.get().getBasicProfile().getEmail());
            commit('setinited', true);
            commit('stoploading', { reason: 'init' }, { root: true });
          });
        }, onerror: () => {
          alert('gapi failed to load');
        }, timeout: 10000, ontimeout: () => {
          alert('gapi load timed out');
        },
      });
    },
    async signin ({ commit, state, dispatch }) {
      commit('startloading', { reason: 'signin' }, { root: true });
      if (!state.inited) throw new Error('Not inited yet');
      const auth = gapi.auth2.getAuthInstance();
      await auth.signIn();
      commit('setsignedin', auth.isSignedIn.get());
      commit('setuser', auth.currentUser.get().getBasicProfile().getEmail().replace(/@.*$/, ''));
      auth.isSignedIn.listen(value => {
        commit('setsignedin', value);
      });
      auth.currentUser.listen(googleUser => {
        commit('setuser', googleUser.getBasicProfile().getEmail().replace(/@.*$/, ''));
      });
      commit('stoploading', { reason: 'signin' }, { root: true });
    },
    async signout ({ commit, state }) {
      const auth = gapi.auth2.getAuthInstance();
      await auth.signOut();
      commit('setsignedin', auth.isSignedIn.get());
      commit('setuser', auth.currentUser.get().getBasicProfile().getEmail().replace(/@.*$/, ''));
    },
  },
};

export function getAuthHeader () { return 'BEARER ' + gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token;}
