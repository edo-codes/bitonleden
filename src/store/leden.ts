import axios from 'axios';
import { getAuthHeader } from './googleauth';
import { DiffPatcher } from 'jsondiffpatch';
import EventBus from '../EventBus';
import { Lid } from '../types/lid';
import { Module } from 'vuex';
import { IRootStore } from './index';

const diffPatcher = new DiffPatcher();
(window as any).diffPatcher = diffPatcher;

export interface ILedenState {
  leden: Lid[],
  loading: string[],
  loadinglid: number,
  searchfunc: string,
  filterlidnrs: number[] | null,
}

export default <Module<ILedenState, IRootStore>>{
  namespaced: true,
  state: {
    leden: [],
    loading: [],
    loadinglid: 0,
    searchfunc: '',
    filterlidnrs: null,
  },
  mutations: {
    setleden (state, leden) {
      state.leden.splice(0, state.leden.length, ...leden);
    },
    setlid (state, lid) {
      const index = state.leden.findIndex(l => l._id === lid._id);
      if (index !== -1)
        state.leden.splice(index, 1, lid);
      else
        state.leden.push(lid);
    },
    deletelid (state, lidnr) {
      state.leden.splice(state.leden.findIndex(l => l._id === lidnr), 1);
    },
    startloadinglid (state) {
      state.loadinglid++;
    },
    stoploadinglid (state) {
      state.loadinglid--;
    },
    startloading (state, { reason }) {
      state.loading.push(reason);
    },
    stoploading (state, { reason }) {
      state.loading.splice(state.loading.indexOf(reason));
    },
    setsearchfunc (state, searchfunc) {
      state.searchfunc = searchfunc;
    },
    setfilterlidnrs (state, lidnrs) {
      state.filterlidnrs = lidnrs;
    },
  },
  actions: {
    async updatelid ({ commit, state, dispatch }, lid) {
      commit('startloadinglid');
      try {
        console.log('lid opslaan: ' + lid._id);
        const delta = diffPatcher.diff(state.leden.find(l => l._id === lid._id), lid);
        console.log(delta);
        if (!delta) return;
        const { data } = await axios.post(`/api/leden/${lid._id}`, delta, { headers: { Authorization: getAuthHeader() } });
        commit('setlid', data);
        EventBus.$emit('opgeslagen');
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploadinglid');
      }
    },
    async deletelid ({ commit, state }, lidnr) {
      commit('startloading', { reason: 'deletelid' });
      try {
        await axios.delete(`/api/leden/${lidnr}`, { headers: { Authorization: getAuthHeader() } });
        commit('deletelid', lidnr);
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploading', { reason: 'deletelid' });
      }
    },
    async loadlid ({ dispatch, commit, state, getters, rootState }, lidnr: number) {
      if (typeof (lidnr) !== 'number') throw Error('loadlid lidnummer not a number');
      commit('startloadinglid');
      try {
        const { data } = await axios.get(`/api/leden/${lidnr}`, { headers: { Authorization: getAuthHeader() } });
        commit('setlid', data);
      } catch (error) {
        console.error(error);
        if (error.response.status === 404) {
          console.log('nieuw lid: ' + lidnr);
          await dispatch('updatelid', { _id: lidnr });
        } else {
          console.error(error.response);
          throw Error(error);
        }
      } finally {
        commit('stoploadinglid');
      }
    },
    async loadleden ({ dispatch, commit, state }) {
      commit('startloading', { reason: 'loadleden' });
      try {
        const { data } = await axios.get('/api/leden', { headers: { Authorization: getAuthHeader() } });
        commit('setleden', data);
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploading', { reason: 'loadleden' });
      }
    },
    async replaceleden ({ commit, dispatch, state }, leden) {
      commit('startloading', { reason: 'replaceleden' });
      try {
        await axios.post(`/api/leden`, leden, { headers: { Authorization: getAuthHeader() } });
        await dispatch('loadleden');
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploading', { reason: 'replaceleden' });
      }
    },
    async filter ({ commit, dispatch, state }, query: string) {
      commit('startloading', { reason: 'filter' });
      try {
        if (query.trim().length === 0 || /{\s*}/.test(query.trim())) {
          commit('setfilterlidnrs', null);
        } else {
          const { data } = await axios.get(`/api/leden/query/${query}`, { headers: { Authorization: getAuthHeader() } });
          commit('setfilterlidnrs', data);
        }
      } catch (e) {
        console.error(e);
        throw Error(e);
      } finally {
        commit('stoploading', { reason: 'filter' });
      }
    },
  },
};