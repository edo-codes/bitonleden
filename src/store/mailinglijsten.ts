/*global gapi*/

import * as config from '../../config.js';
import { Module } from 'vuex';
import { IRootStore } from './index';
import { ILedenState } from './leden';
import { Lid } from '../types/lid';

export interface IMailinglijstenState {
  loading: string[],
  loadingmailinglijstleden: string[],
  mailinglijsten: Mailinglijst[],
  byemail: {
    email: string,
    mailinglijsten: {
      Id: string,
      Emailadres: string,
      Naam: string
    }[]
  }
}

export interface Mailinglijst {
  Id: string;
  Emailadres: string;
  Naam: string;
  Leden: {
    Emailadres: string
    Lidnummer?: number
    Geenlid: boolean
    Oudlid: boolean
    Bitonnaam?: string
  }[]
}

export default <Module<IMailinglijstenState, IRootStore>>{
  namespaced: true,
  state: {
    loading: [],
    loadingmailinglijstleden: [],
    mailinglijsten: [],
    byemail: {
      email: '',
      mailinglijsten: [],
    },
  },
  mutations: {
    startloading (state, { reason }) {
      state.loading.push(reason);
    },
    stoploading (state, { reason }) {
      state.loading.splice(state.loading.indexOf(reason));
    },
    startloadingmailinglijstleden (state, { reason }) {
      state.loadingmailinglijstleden.push(reason);
    },
    stoploadingmailinglijstleden (state, { reason }) {
      state.loadingmailinglijstleden.splice(state.loading.indexOf(reason));
    },
    clearmailinglijsten (state) {
      state.mailinglijsten.splice(0, state.mailinglijsten.length);
    },
    pushmailinglijsten (state, mailinglijsten) {
      state.mailinglijsten.push.apply(state.mailinglijsten, mailinglijsten);
    },
    clearmailinglijstleden (state, id) {
      const mailinglijst = state.mailinglijsten.find(m => m.Id.toLowerCase() === id.toLowerCase())!;
      mailinglijst.Leden.splice(0, mailinglijst.Leden.length);
    },
    pushmailinglijstleden (state, { id, leden }) {
      const mailinglijst = state.mailinglijsten.find(m => m.Id.toLowerCase() === id.toLowerCase())!;
      mailinglijst.Leden.push.apply(mailinglijst.Leden, leden);
    },
    clearmailinglijstenbyemail (state, { email }) {
      state.byemail.email = email;
      state.byemail.mailinglijsten.splice(0, state.byemail.mailinglijsten.length);
    },
    pushmailinglijstenbyemail (state, { email, mailinglijsten }) {
      state.byemail.email = email;
      state.byemail.mailinglijsten.push.apply(state.byemail.mailinglijsten, mailinglijsten);
    },
  },
  actions: {
    async loadmailinglijsten ({ commit, state, dispatch }) {
      commit('startloading', { reason: 'loadmailinglijsten' });
      commit('clearmailinglijsten');
      let response = null;
      let nextPageToken = null;
      do {
        response = await gapi.client.directory.groups.list({
          customer: 'my_customer',
          fields: 'groups(email,name),nextPageToken',
          pageToken: nextPageToken,
        });
        if (response.result.groups)
          commit('pushmailinglijsten', response.result.groups.map(({ email, name }: { email: string, name: string }) => ({
            Id: (new RegExp('(.*)@')).exec(email)![1],
            Emailadres: email,
            Naam: name,
            Leden: [],
          })));
      } while (response.result.nextPageToken && (nextPageToken = response.result.nextPageToken));
      commit('stoploading', { reason: 'loadmailinglijsten' });
    },
    async loadmailinglijstleden ({ commit, state, dispatch, getters, rootState }, id) {
      const mailinglijst = state.mailinglijsten.find(m => m.Id.toLowerCase() === id.toLowerCase());
      if (!mailinglijst) return;
      commit('startloadingmailinglijstleden', { reason: id });
      commit('clearmailinglijstleden', id);
      let response: any | null = null;
      let nextPageToken = null;
      do {
        response = await gapi.client.request({
          path: `https://www.googleapis.com/admin/directory/v1/groups/${encodeURIComponent(mailinglijst.Emailadres)}/members`,
          params: {
            fields: 'members/email,nextPageToken',
            pageToken: nextPageToken,
          },
        });
        if (response.result.members)
          commit('pushmailinglijstleden', {
            id, leden: response.result.members.map(({ email }: { email: string }) => {
              const lid = ((rootState as any).leden as ILedenState).leden.find(l => !!l.Emailadres && l.Emailadres.toLowerCase() === email.toLowerCase()) as Lid;
              return ({
                Emailadres: email,
                Lidnummer: lid ? lid._id : undefined,
                Geenlid: !lid && !email.endsWith('@' + config.gapi_domain),
                Oudlid: lid && !!lid.Opzegdatum,
                Bitonnaam: lid ? lid.Bitonnaam : undefined,
              });
            }),
          });
      } while (response.result.nextPageToken && (nextPageToken = response.result.nextPageToken));
      commit('stoploadingmailinglijstleden', { reason: id });
    },
    async loadmailinglijstenbyemail ({ commit, state, dispatch, getters, rootState }, email) {
      commit('startloading', { reason: 'loadmailinglijstenbyemail' });
      commit('clearmailinglijstenbyemail', { email });
      try {
        let response = null;
        let nextPageToken = null;
        do {
          response = await gapi.client.directory.groups.list({
            domain: config.gapi_domain,
            fields: 'groups(email,name),nextPageToken',
            userKey: email,
            pageToken: nextPageToken,
          });
          if (response.result.groups)
            commit('pushmailinglijstenbyemail', {
              email,
              mailinglijsten: response.result.groups.map(({ email, name }: { email: string, name: string }) => ({
                Id: (new RegExp('(.*)@')).exec(email)![1],
                Emailadres: email,
                Naam: name,
              })),
            });
        } while (response.result.nextPageToken && (nextPageToken = response.result.nextPageToken));
      } catch (e) {
        console.error(e);
        commit('clearmailinglijstenbyemail', { email: email });
      } finally {
        commit('stoploading', { reason: 'loadmailinglijstenbyemail' });
      }
    },
    async addmailinglijstleden ({ commit, state, dispatch, getters, rootState }, { mailinglijst, emails }: { mailinglijst: Mailinglijst, emails: string[] }) {
      if (mailinglijst.Emailadres !== 'devtest@biton.nl') throw new Error();
      commit('startloadingmailinglijstleden', { reason: 'addmailinglijstleden' });
      console.log(emails);
      const batch = gapi.client.newBatch();
      for (let email of emails) {
        batch.add(gapi.client.request({
          path: `https://www.googleapis.com/admin/directory/v1/groups/${encodeURIComponent(mailinglijst.Emailadres)}/members`,
          method: 'POST',
          body: {
            'email': email,
          },
        }));
      }
      try {
        const result = await batch;
        const dispresult = [];
        for (let e of Object.values(result.result).entries()) {
          const a = emails[e[0]] + ': ';
          if (e[1].status && e[1].status === 200)
            dispresult.push(a + 'Toegevoegd');
          else if (e[1].result && (e[1].result as any).error && ((e[1].result as any).error as any).message)
            dispresult.push(a + (e[1].result as any).error.message);
          else if (e[1].body)
            dispresult.push(a + JSON.stringify(JSON.parse(e[1].body)));
          else
            dispresult.push(a + JSON.stringify(e[1]));
        }
        dispatch('loadmailinglijstleden', mailinglijst.Id);
        return dispresult.join('\n');
      } catch (e) {
        throw e;
      } finally {
        commit('stoploadingmailinglijstleden', { reason: 'addmailinglijstleden' });
      }
    },
    async removemailinglijstleden ({ commit, state, dispatch, getters, rootState }, { mailinglijst, emails }: { mailinglijst: Mailinglijst, emails: string[] }) {
      if (mailinglijst.Emailadres !== 'devtest@biton.nl') throw new Error();
      commit('startloadingmailinglijstleden', { reason: 'removemailinglijstleden' });
      const batch = gapi.client.newBatch();
      for (let email of emails) {
        batch.add(gapi.client.directory.members.delete({
          groupKey: mailinglijst.Emailadres,
          memberKey: email,
        }));
      }
      try {
        const result = await batch;
        const dispresult = [];
        for (let e of Object.values(result.result).entries()) {
          const a = emails[e[0]] + ': ';
          if (e[1].status && (e[1].status === 200 || e[1].status === 204))
            dispresult.push(a + 'Verwijderd');
          else if (e[1].result && (e[1].result as any).error && ((e[1].result as any).error as any).message)
            dispresult.push(a + (e[1].result as any).error.message);
          else if (e[1].body)
            dispresult.push(a + JSON.stringify(JSON.parse(e[1].body)));
          else
            dispresult.push(a + JSON.stringify(e[1]));
        }
        dispatch('loadmailinglijstleden', mailinglijst.Id);
        return dispresult.join('\n');
      } catch (e) {
        throw e;
      } finally {
        commit('stoploadingmailinglijstleden', { reason: 'removemailinglijstleden' });
      }
    },
  },
};
