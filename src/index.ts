import Vue from 'vue';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';
import SuiVue from 'semantic-ui-vue';
import Loading from './Loading.vue';
import VueObserveVisibility from 'vue-observe-visibility';
import * as PortalVue from 'portal-vue';
import router from './router';
import store from './store/index';

Vue.use(BootstrapVue);
Vue.use(SuiVue);
Vue.use(VueObserveVisibility);
Vue.use(PortalVue);

Vue.component('loading', Loading);

Vue.config.errorHandler = (err, vm, info) => {
  console.error('Error Handler: ', err, vm, info);
};

new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
});
