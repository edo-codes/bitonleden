import Vue from 'vue';
import Router from 'vue-router';
import AppNav from './AppNav.vue';
import Empty from './Empty.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      components: {
        side: AppNav,
      },
    }, {
      path: '/leden',
      name: 'leden',
      components: {
        side: AppNav,
        a: () => import('./leden/LedenList.vue'),
      },
    }, {
      path: '/leden/:lidnr',
      components: {
        side: AppNav,
        a: () => import('./leden/LedenList.vue'),
        b: () => import('./leden/LidDetails.vue'),
      },
      children: [
        {
          path: '',
          name: 'leden-lidnr',
          component: () => import('./leden/LidForm.vue'),
        },
        {
          path: 'json',
          name: 'leden-lidnr-json',
          component: () => import('./leden/LidJson.vue'),
        },
        {
          path: 'mailinglijsten',
          name: 'leden-lidnr-mailinglijsten',
          component: () => import('./leden/LidMailinglijsten.vue'),
        },
        {
          path: 'stamboom',
          name: 'leden-lidnr-stamboom',
          component: () => import('./leden/LidStamboom.vue'),
        },
      ],
    }, {
      path: '/leden/import',
      name: 'leden-import',
      components: {
        side: AppNav,
        a: Empty,
        b: () => import('./leden/LedenImport.vue'),
      },
    },
    {
      path: '/gegevenscontrole',
      name: 'gegevenscontrole',
      components: {
        side: AppNav,
        a: Empty,
        b: () => import('./gegevenscontrole/StartGegevenscontrole.vue'),
      },
    },
    {
      path: '/gegevenscontrole/:lidnr/:code',
      name: 'gegevenscontrole-lidnr-code',
      components: {
        guest: () => import('./gegevenscontrole/InvulGegevenscontrole.vue'),
      },
    },
    {
      path: '/commissies/',
      name: 'commissies',
      components: {
        side: AppNav,
        a: () => import('./commissies/CommissiesList.vue'),
      },
    },
    {
      path: '/commissies/:naam',
      name: 'commissies-naam',
      components: {
        side: AppNav,
        a: () => import('./commissies/CommissiesList.vue'),
        b: () => import('./commissies/CommissieDetails.vue'),
      },
    },
    {
      path: '/mailinglijsten',
      name: 'mailinglijsten',
      components: {
        side: AppNav,
        a: () => import('./mailinglijsten/MailinglijstenList.vue'),
      },
    },
    {
      path: '/mailinglijsten/:id',
      name: 'mailinglijsten-id',
      components: {
        side: AppNav,
        a: () => import('./mailinglijsten/MailinglijstenList.vue'),
        b: () => import('./mailinglijsten/MailinglijstDetails.vue'),
      },
    },
    {
      path: '/logboeken',
      name: 'logboeken',
      redirect: { name: 'logboeken-mailinglijsten' },
    },
    {
      path: '/logboeken/leden',
      name: 'logboeken-leden',
      components: {
        side: AppNav,
        a: Empty,
        b: () => import('./logboeken/LogboekLeden.vue'),
      },
    }, {
      path: '/logboeken/mailinglijsten',
      name: 'logboeken-mailinglijsten',
      components: {
        side: AppNav,
        a: Empty,
        b: () => import('./logboeken/LogboekMailinglijsten.vue'),
      },
    }, {
      path: '*',
      components: {
        side: AppNav,
        a: Empty,
        b: {
          render: (h) => h('span', ['404 Not Found']),
        },
      },
    },
  ],
  mode: 'history',
});
