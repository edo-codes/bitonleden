module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 8,
    sourceType: 'module',
    ecmaFeatures: {
      impliedStrict: true,
    },
  },
  parser: 'typescript-eslint-parser',
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: 'eslint:recommended',
  // required to lint *.vue files
  plugins: [
    'html',
  ],
  // add your custom rules here
  rules: {
    'indent': ['warn', 2],
    'no-console': 'off',
    'no-unused-vars': 'off',
    'comma-dangle': ['error', 'always-multiline'],
  },
  globals: {
    'Vue': true,
    'gapi': true,
  },
};
