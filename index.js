const fs = require('fs');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const Bundler = require('parcel-bundler');
const { compileFromFile } = require('json-schema-to-typescript');

console.log('Compiling Lid schema');
compileFromFile('lidschema.json').then(ts => {
  fs.writeFileSync('./src/types/lid.d.ts', ts);
});

const app = express();

console.log('Starting servers on 8080');

let apiRouter = require('./api').apiRouter;
app.use('/api', (req, res, next) => {
  apiRouter(req, res, next);
});
console.log('Serving api');

app.use(bodyParser.text({ type: 'text/plain' }));
app.use(bodyParser.json({ limit: '50mb' }));

app.use('/', express.static('./static'));
console.log('Serving ./static/');

const bundler = new Bundler('./src/index.html', {
  publicUrl: '',
});
console.log('Serving Parcel');
app.use(bundler.middleware());

app.listen(8080);