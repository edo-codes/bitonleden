const Router = require('express').Router;
const model = require('./model');
const Ajv = require('ajv');
const lidschema = require('../lidschema');
const { authmiddleware } = require('./auth.js');
const router = Router();
const ajv = new Ajv();
const { diff, patch } = require('jsondiffpatch');

router.use('/leden', authmiddleware);

router.get('/leden', async function (req, res, next) {
  try {
    const leden = await model.leden.cfind({}).projection({
      _id: 1,
      Bitonnaam: 1,
      PaMa_Lidnummer: 1,
      Naam: 1,
      Emailadres: 1,
      Opzegdatum: 1,
    }).exec();
    return res.json(leden);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get('/leden/log', async function (req, res) {
  try {
    const deltas = await model.deltas.cfind({}).sort({ date: -1 }).exec();
    return res.json(deltas);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.post('/leden', async function (req, res, next) {
  const leden = req.body;
  try {
    await model.leden.remove({}, { multi: true });
    leden.forEach(async lid => {
      await model.leden.insert(lid);
    });
    return res.sendStatus(200);
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.get('/leden/query/:query', async function (req, res, next) {
  let query;
  try {
    query = JSON.parse(req.params.query);
  } catch (e) {
    return res.status(400).send(e);
  }
  console.log(JSON.stringify(query));
  try {
    const leden = await model.leden.find(query, { _id: 1 });
    const lidnrs = leden.map(({ _id }) => _id);
    return res.json(lidnrs);
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.get('/leden/:lidnr', async function (req, res, next) {
  const lidnr = parseInt(req.params.lidnr);
  if (lidnr <= 0) return res.sendStatus(400);
  try {
    const lid = await model.leden.findOne({ _id: lidnr });
    if (lid) {
      return res.json(lid);
    } else {
      return res.sendStatus(404);
    }
  } catch (e) {
    console.error(e);
    return res.status(500).send(e);
  }
});

router.post('/leden/:lidnr', async function (req, res, next) {
  const lidnr = parseInt(req.params.lidnr);
  if (lidnr <= 0) return res.status(400).send('lidnr must be >= 1');
  const delta = req.body;
  try {
    const oudlid = await model.leden.findOne({ _id: lidnr });
    const nieuwlid = patch(oudlid, delta);
    if (!ajv.validate(lidschema, nieuwlid)) return res.status(400).send(ajv.errorsText);
    await model.deltas.insert({ lidnr, date: new Date(), delta });
    await model.leden.update({ _id: lidnr }, nieuwlid, { upsert: true });
    return res.json(nieuwlid);
  } catch (e) {
    console.error(e);
    return res.status(500).send(e);
  }
});

router.delete('/leden/:lidnr', async function (req, res) {
  const lidnr = parseInt(req.params.lidnr);
  if (lidnr <= 0) return res.status(400).send('lidnr moet >= 1 zijn');
  try {
    const result = await model.leden.remove({ _id: lidnr });
    console.log(result);
    res.sendStatus(200);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
