const Router = require('express').Router;
const leden = require('./leden');
const commissies = require('./commissies');
const gegevenscontrole = require('./gegevenscontrole');
const bodyParser = require('body-parser');

const router = Router();
router.use(bodyParser.json({ limit: '100mb' }));
router.get('/', (req, res, next) => {
  res.json({
    leden: '/api/leden',
    commissies: '/api/commissies',
    gegevenscontrole: '/api/gegevenscontrole',
  });
});
router.use(leden);
router.use(commissies);
router.use(gegevenscontrole);

module.exports.apiRouter = router;
