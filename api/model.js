const Datastore = require('nedb-promise');

const db = {
  leden: new Datastore({ filename: 'ledenlijstleden.db', autoload: true }),
  deltas: new Datastore({ filename: 'ledenlijstdeltas.db', autoload: true }),
  commissies: new Datastore({ filename: 'ledenlijstcommissies.db', autoload: true }),
  authaccounts: new Datastore({ filename: 'authaccounts.db', autoload: true }),
  gegevenscontrolecodes: new Datastore({ filename: 'gegevenscontrolecode.db', autoload: true }),
};

db.commissies.ensureIndex({ fieldName: 'Naam', unique: true });

module.exports = db;