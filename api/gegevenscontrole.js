const Router = require('express').Router;
const model = require('./model');
const Ajv = require('ajv');
const lidschema = require('../lidschema');
const bodyParser = require('body-parser');
const { verifyToken } = require('./auth');
const router = Router();
const ajv = new Ajv();

router.get('/gegevenscontrole/:lidnr/:code', async function (req, res, next) {
  const lidnr = parseInt(req.params.lidnr);
  const code = req.params.code;
  console.log(lidnr, code);
  try {
    const result = await model.gegevenscontrolecodes.findOne({ _id: lidnr });
    if (!result) return res.sendStatus(404);
    console.log(result);
    if (result.code !== code)
      return res.sendStatus(403);
    else {
      const lid = await model.leden.findOne({ _id: lidnr });
      if (lid) {
        return res.json(lid);
      } else {
        return res.sendStatus(404);
      }
    }
  } catch (e) {
    console.error(e);
    return res.status(500).send(e);
  }
});

// 'code', niet ':code'
router.post('/gegevenscontrole/:lidnr/code', bodyParser.text({ type: 'text/plain' }), async function (req, res, next) {
  const lidnr = parseInt(req.params.lidnr);
  const code = req.body.trim();
  console.log(code);
  try { await verifyToken(req.get('authorization')); } catch (e) { return res.status(403).send(e); }
  try {
    if (code.length < 5) { res.status(400).send('Code te kort');}
    else {
      const doc = { _id: lidnr, code: code };
      console.log(JSON.stringify(doc));
      await model.gegevenscontrolecodes.update({ _id: lidnr }, doc, { upsert: true });
      res.sendStatus(200);
    }
  } catch (e) {
    console.error(e);
    return res.status(500).send(e);
  }
});

router.post('/gegevenscontrole/:lidnr/:code', async function (req, res, next) {
  const lidnr = parseInt(req.params.lidnr);
  const code = req.params.code;
  try {
    const result = await model.gegevenscontrolecodes.findOne({ _id: lidnr });
    if (result.code !== code)
      return res.status(403).send('Onjuiste code');
    else {
      const lid = req.body;
      if (lid._id !== lidnr) return res.status(400).send('lid._id ≠ lidnr in url');
      if (!ajv.validate(lidschema, lid)) return res.status(400).send(ajv.errorsText);
      await model.leden.update({ _id: lidnr }, lid, { upsert: true });
      return res.sendStatus(200);
    }
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.get('/gegevenscontrole/codes', async function (req, res, next) {
  try {
    try { await verifyToken(req.get('authorization')); } catch (e) { return res.status(403).send(e); }
    const result = await model.gegevenscontrolecodes.find({});
    return res.json(result);
  } catch (e) {
    return res.status(500).send(e);
  }
});

module.exports = router;
