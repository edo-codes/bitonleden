const Router = require('express').Router;
const model = require('./model');
const Ajv = require('ajv');
const lidschema = require('../lidschema');
const { authmiddleware } = require('./auth.js');

const router = Router();
const ajv = new Ajv();

router.use('/commissies', authmiddleware);

router.get('/commissies', async function (req, res, next) {
  try {
    const commissies = await model.commissies.cfind({}).sort({ Naam: 1 }).exec();
    return res.json(commissies);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get('/commissies/:naam', async function (req, res, next) {
  const naam = req.params.naam;
  if (!naam) return res.sendStatus(400);
  try {
    const commissie = await model.commissies.findOne({ Naam: naam });
    if (commissie) {
      return res.json(commissie);
    } else {
      return res.sendStatus(404);
    }
  } catch (e) {
    return res.send(500).send(e);
  }
});

router.post('/commissies/:naam', async function (req, res, next) {
  const naam = req.params.naam;
  if (!naam) return res.sendStatus(400);

  const commissie = req.body;
  //if (!ajv.validate(lidschema, commissie)) return res.status(400).send(ajv.errorsText)
  if (commissie.Naam !== naam) return res.sendStatus(400).send('Naam in object != naam in URL');
  try {
    await model.commissies.update({ Naam: naam }, commissie, { upsert: true });
    res.sendStatus(200);
  } catch (e) {
    return res.status(500).send(e);
  }
});

router.delete('/commissies/:naam', async function (req, res) {
  const naam = req.params.naam;
  if (!naam) return res.status(400);
  try {
    await model.commissies.remove({ Naam: naam });
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
