const GoogleAuth = require('google-auth-library');
const model = require('./model');
const config = require('../config');

async function verifyToken (authHeader) {
  const id_token = (authHeader || '').split(/[ ]+/)[1];
  if (!id_token) throw Error('Authorization header not set');
  const payload = await new Promise((resolve, reject) => {
    (new (new GoogleAuth).OAuth2(config.gapi_client_id, '', '')).verifyIdToken(id_token, config.gapi_client_id, (e, login) => {
      if (e) {
        return reject(e);
      } else {
        return resolve(login.getPayload());
      }
    });
  });
  if ((await model.authaccounts.find({ _id: payload.email })).length === 0) {
    console.error('Account not allowed: ' + payload.email);
    throw Error('Account not allowed: ' + payload.email);
  } else {
    return true;
  }
}

const authmiddleware = async (req, res, next) => {
  try {
    await verifyToken(req.get('authorization'));
    next();
  } catch (e) { return res.status(403).send(e.message); }
};

module.exports = {
  verifyToken,
  authmiddleware,
};
