# B.I.T.O.N. Ledenlijst

WIP webapplicatie voor het opslaan en bijhouden van de ledenadministratie van Vereniging B.I.T.O.N.

Tech: TypeScript, Vue 2, Vuex, Express.js, NeDB, Brutisin json-forms

Features:
* Ledenbestand
* Integratie mailinglijsten met Google Groups
* Commissiebestand
* Gegevenscontrole sturen naar leden
* Visualisatie stamboom
* Onthoudt historie van alle wijzigingen
