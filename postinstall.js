const { exec } = require('child_process');
const fs = require('fs');

exec('cd node_modules/parcel-bundler; yarn; yarn build;').stdout.pipe(process.stdout);

//delete .babelrc
exec('find ./node_modules -not -path "./node_modules/parcel-bundler/*" -name "*.babelrc" -type f -print -delete').stdout.pipe(process.stdout);

//delete babel section in portal-vue package.json
const content = fs.readFileSync('./node_modules/portal-vue/package.json', 'utf8');
const newContent = content.replace(
  '  "babel": {\n    "presets": [\n      [\n        "es2015",\n        {\n          "modules": false\n        }\n' +
  '      ],\n      "stage-3"\n    ],\n    "plugins": ["transform-vue-jsx"],\n    "comments": true,\n    "env": {\n' +
  '      "jest": true,\n      "test": {\n        "presets": ["es2015", "stage-3"]\n      }\n    }\n  },\n', '');
fs.writeFileSync('./node_modules/portal-vue/package.json', newContent, 'utf8');
